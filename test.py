import getopt
import sys


def get_mode():
    this_mode = 'production'
    for opt, arg in opts:
        if opt == '-h':
            print('test.py -m <mode[debug,production]>')
            sys.exit()
        elif opt in ("-m", "--mode"):
            this_mode = arg
    return this_mode


################################################################
def get_words():
    word = input('Add a word: ')
    if word == '':
        return
    wordList.append(word)
    return get_words()


################################################################
def get_matches():
    these_matches = input('Number of matches: ')
    if these_matches == '':
        print('Now ur a πr8!!!!!!!1!!!')
        sys.exit()
    try:
        int(these_matches)
    except ValueError:
        print("NaN")
        return get_matches()
    return these_matches


################################################################
def compare_words(word1, word2):
    these_matches = 0
    for i, char in enumerate(word1):
        if char == word2[i]:
            these_matches += 1
    return these_matches


################################################################
def get_best_word():
    word_dict = {}
    for word in wordList:
        word_dict[word] = 0

    for i in range(0, len(wordList) - 1):
        for j in range(i + 1, len(wordList)):
            result = compare_words(wordList[i], wordList[j])
            word_dict[wordList[i]] += result
            word_dict[wordList[j]] += result

    result_dict = {}
    for word in word_dict:
        result = word_dict[word]
        if result in result_dict:
            result_dict[result].append(word)
        else:
            result_dict[result] = [word]

    total = sum(list(result_dict.keys()))

    if len(result_dict) == 0:
        print('Error 50L: Paddle not found')
        sys.exit()
    elif len(result_dict) == 1 and len(result_dict[list(result_dict.keys())[0]]) == 1:
        print('That leaves ' + result_dict[list(result_dict.keys())[0]][0].upper())
        sys.exit()

    if mode == 'debug':
        print(result_dict)

    best = list(sorted(result_dict.keys()))[-1]
    percentage = ((best / total) / len(result_dict[best])) * 100

    if mode == 'debug':
        print(str(int(percentage)) + "%")

    return result_dict[best][0]


################################################################
def refine_word_list(list_to_refine, match_word, match_count):
    refined_word_list = []
    for word in list_to_refine:
        result = compare_words(match_word, word)
        if int(result) == int(match_count):
            refined_word_list.append(word)
    return refined_word_list


################################################################

try:
    opts, args = getopt.getopt(sys.argv[1:], "hm:", ["mode="])
except getopt.GetoptError:
    print('test.py -m <mode[debug,production]>')
    sys.exit(2)

mode = get_mode()
if mode == 'production':
    wordList = []
    get_words()
    if len(wordList) == 0:
        print('Word to your mother!')
        sys.exit()
else:
    wordList = ["test", "best", "food", "beat", "crum", "dude", "beer", "wine", "shot", "ball", "wall", "tall", "fall",
                "ward", "bard", "turd", "word", "fist", "mist", "fast", "bask", "mask"]

matches = 0
while 1 == 1:
    suggestion = get_best_word()
    print('Try ' + suggestion.upper())
    numberOfMatches = get_matches()
    wordList = refine_word_list(wordList, suggestion, numberOfMatches)
    print(wordList)
